import * as cdk from 'aws-cdk-lib';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as cloudfront from 'aws-cdk-lib/aws-cloudfront';
import * as cloudfront_origins from 'aws-cdk-lib/aws-cloudfront-origins';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';

export class CloudfrontToManyS3Stack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    const assetsBucketFirst = new s3.Bucket(this, "demo-assets-bucket-first", {
      bucketName: "demo-assets-bucket-first",
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    const assetsBucketSecond = new s3.Bucket(this, "demo-assets-bucket-second", {
      bucketName: "demo-assets-bucket-second",
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    const assetsBucketDefault = new s3.Bucket(this, "demo-assets-bucket-default", {
      bucketName: "demo-assets-bucket-default",
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });



    // Origin request handler.
    const lambdaFunction = new cloudfront.experimental.EdgeFunction(this, "OriginRequestHandler", {
      runtime: lambda.Runtime.NODEJS_14_X,
      handler: 'handler.handler',
      code: lambda.Code.fromAsset( 'lib/lambda'),
    });



    // Create new CloudFront Distribution
    const cf = new cloudfront.Distribution(this, "cdnDistribution", {
      defaultBehavior: { origin: new cloudfront_origins.S3Origin(assetsBucketDefault) },
      additionalBehaviors: {
        '/first/*': {
          origin: new cloudfront_origins.S3Origin(assetsBucketFirst),
          edgeLambdas: [
            {
              eventType: cloudfront.LambdaEdgeEventType.VIEWER_REQUEST,
              functionVersion: lambdaFunction.currentVersion,
                  
              // the properties below are optional
              includeBody: false,
            },
          ],
        },
        '/second/*': {
          origin: new cloudfront_origins.S3Origin(assetsBucketSecond),
          edgeLambdas: [
            {
              eventType: cloudfront.LambdaEdgeEventType.VIEWER_REQUEST,
              functionVersion: lambdaFunction.currentVersion,
                  
              // the properties below are optional
              includeBody: false,
            },
          ],
        },
      },
    });


  }
}
