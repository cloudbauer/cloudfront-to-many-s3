# Welcome to this CDK TypeScript sample!

This is a demo project for TypeScript development with CDK.
It creates CloudFront with multiple S3 origins and Lambda@Edge Functions to remove first path element.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

> **IMPORTANT**
> 
> CDK needs to be bootstraped first in your region!
> Additional it needs to be bootstrapped to region us-east-1, if your region is different.
> (Required for Lambda@Edge)
> 
> You can set account and region with
> `export CDK_DEFAULT_ACCOUNT=123456789012`
> `export AWS_DEFAULT_REGION=eu-central-1`
> 
> If you use `--profile abc` argument together with cdk command you will probably need to set region with
> `export CDK_DEFAULT_REGION=eu-central-1`

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
